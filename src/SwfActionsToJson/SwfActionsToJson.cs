﻿using Newtonsoft.Json.Linq;
using SwfLib;
using SwfLib.Actions;
using SwfLib.Tags;
using SwfLib.Tags.ActionsTags;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace XavLab.SwfUtils
{
    /// <summary>
    /// Reader SWF class.
    /// </summary>
    class SwfDecompilator
    {
        private SwfFile swf;

        public SwfDecompilator(Stream SwfDataFile)
        {
            swf = SwfFile.ReadFrom(SwfDataFile);
        }

        /// <summary>
        /// Append all actions of SWF file to a dedicated Queue.
        /// </summary>
        /// <returns>Queue containing all SWF actions tags.</returns>
        public Queue<ActionBase> GetQueueActionBase()
        {
            Queue<ActionBase> datas = new Queue<ActionBase>();

            List<SwfTagBase> fileTags = (List<SwfTagBase>)swf.Tags;

            foreach (SwfTagBase tag in fileTags)
            {
                if (tag.TagType == SwfTagType.DoAction)
                {
                    DoActionTag actionsTag = (DoActionTag)tag;

                    List<ActionBase> listAction = (List<ActionBase>)actionsTag.ActionRecords;

                    foreach (ActionBase action in listAction)
                    {
                        datas.Enqueue(action);
                    }
                }
            }

            return datas;
        }
    }

    /// <summary>
    /// Utility class which serializing ActionScript code from a SWF file into a formatted JSON object.
    /// </summary>
    public class SwfActionsToJson
    {
        // Constants list, containing all currently read strings of the SWF file
        private List<string> constants = new List<string>();

        // All tag actions of the SWF file
        private Queue<ActionBase> actions;

        // Pre-compiled stack, contain push items before being processed as a JSON object
        private Stack<object> precompiled = new Stack<object>();

        // The current writer (parser)
        private JTokenWriter writer = new JTokenWriter();

        // Referenced Writers
        // When object is created, adding a new writer referenced by his node ("Root.Parent.Child")
        private Dictionary<string, JTokenWriter> writers = new Dictionary<string, JTokenWriter>();

        #region Initialization
        /// <summary>
        /// Initialize instance with SWF datas from stream (e.g. stream of file).
        /// </summary>
        /// <param name="SwfDataFile">SWF datas from stream</param>
        public SwfActionsToJson(Stream SwfDataFile)
        {
            SwfDecompilator sd = new SwfDecompilator(SwfDataFile);
            actions = sd.GetQueueActionBase();

            Initialize();
        }

        private void Initialize()
        {
            // First add root writer
            writers.Add("root", new JTokenWriter());

            // Current writer becomes the root writer
            writer = writers["root"];
            writer.WriteStartObject();
        }
        #endregion

        /// <summary>
        /// Parse SWF Actions to a JSON Object.
        /// </summary>
        /// <returns>Return formatted JSON Object from SWF Actions.</returns>
        public JObject GetJson()
        {
            ActionBase current;

            while (actions.Count > 0)
            {
                current = actions.Dequeue();
                ProcessAction(current);
            }

            return BuildJson();
        }

        #region Json Serializer
        private JObject BuildJson()
        {
            // Build from "root"
            string key = "root";

            // Set the root writer to JObject
            JObject root = (JObject)writers[key].Token;

            // For each writers (they are objects), append the writer to root corresponding node
            foreach (string w in writers.Keys)
            {
                // We recovered a object
                if (w != key)
                {
                    // Prevent empty writer
                    if (writers[w].Token.GetType() == typeof(JObject))
                    {
                        // Replace the root referenced node by the current writer (w)
                        root.SelectToken(w).Replace(writers[w].Token);
                    }
                }
            }

            return root;
        }

        private void ProcessAction(ActionBase action)
        {
            switch (action.ActionCode)
            {
                // Get the new constants
                case ActionCode.ConstantPool:
                    {
                        constants.Clear();
                        ActionConstantPool constantPool = (ActionConstantPool)action;
                        constants.AddRange((List<string>)constantPool.ConstantPool);
                        break;
                    }

                // Store item in precompiled stack for future use
                case ActionCode.Push:
                    {
                        ActionPush pushAction = (ActionPush)action;

                        foreach (ActionPushItem item in pushAction.Items)
                        {
                            precompiled.Push(GetValuePushItem(item));
                        }

                        break;
                    }

                // SetVariable is used only on root
                case ActionCode.SetVariable:
                    {
                        writer = writers["root"];

                        object value = precompiled.Pop();

                        string name = (string)precompiled.Pop();

                        writer.WritePropertyName(name);

                        // New Array
                        if (value.GetType() == typeof(object[]))
                        {
                            WriteMultiDimensionnalArray((object[])value);
                        }
                        // New Object, so add new writer referenced by PropertyName
                        else if (value.GetType() == typeof(object))
                        {
                            writers.Add(name, new JTokenWriter());

                            writer = writers[name];
                            writer.WriteStartObject();

                            writer = writers["root"];
                            writer.WriteNull();
                        }
                        else
                        {
                            // Set new primitive value 
                            writer.WriteValue(value);
                        }

                        break;
                    }

                // Same as SetVariable, expect is used only into new object (!= root)
                case ActionCode.SetMember:
                    {
                        object value = precompiled.Pop();

                        object name = precompiled.Pop();

                        // If we are in SWF Dict like MyDict[1], MyDict[3].
                        // The referenced number becomes the property name
                        if (name.GetType() == typeof(int))
                        {
                            name = Convert.ToString(name);
                        }
                        else if (name.GetType() == typeof(double))
                        {
                            name = Convert.ToString(name);
                        }

                        writer.WritePropertyName((string)name);

                        if (value.GetType() == typeof(object[]))
                        {
                            WriteMultiDimensionnalArray((object[])value);
                        }
                        // If we have a new JObject nested in 
                        else if (value.GetType() == typeof(JObject))
                        {
                            JObject v = (JObject)value;

                            foreach (var x in v)
                            {
                                if (x.Value.GetType() == typeof(JObject))
                                {
                                    JObject o = (JObject)x.Value;

                                    if (o.Count == 0)
                                    {
                                        // Add this current parent node if need in future
                                        string parent = writers.FirstOrDefault(y => y.Value == writer).Key;
                                        string writerName = parent + '.' + (string)name;
                                        writers.Add(writerName, new JTokenWriter());
                                        writer = writers[writerName];
                                        writer.WriteNull();
                                        writer = writers[parent];

                                        // Add this child node
                                        writerName = parent + '.' + (string)name + '.' + x.Key;
                                        writers.Add(writerName, new JTokenWriter());
                                        writer = writers[writerName];
                                        writer.WriteStartObject();
                                        writer = writers[parent];
                                    }
                                }
                            }

                            writer.WriteToken(v.CreateReader());
                        }
                        // New Object, so add new writer referenced by PropertyName
                        else if (value.GetType() == typeof(object))
                        {
                            string parent = writers.FirstOrDefault(x => x.Value == writer).Key;
                            string writerName = parent + '.' + (string)name;

                            writers.Add(writerName, new JTokenWriter());
                            writer = writers[writerName];
                            writer.WriteStartObject();

                            writer = writers[parent];
                            writer.WriteNull();
                        }
                        else
                        {
                            writer.WriteValue(value);
                        }

                        break;
                    }

                // Recover the size and items of array from precompiled stack, 
                // assembling it then re-push to the precompiled stack
                case ActionCode.InitArray:
                    {
                        int size = Convert.ToInt32(precompiled.Pop());

                        object[] array = new object[size];

                        for (int i = 0; i < size; i++)
                        {
                            array[i] = precompiled.Pop();
                        }

                        precompiled.Push(array);

                        break;
                    }

                // Initialize object then construct manually from precompiled stack
                case ActionCode.InitObject:
                    {
                        int size = Convert.ToInt32(precompiled.Pop());

                        JObject obj = InitObject(size);

                        precompiled.Push(obj);

                        break;
                    }

                // Push object type at the top of precompiled stack
                case ActionCode.NewObject:
                    {
                        // Not used for our purpose
                        string type = (string)precompiled.Pop();

                        // Not used for our purpose
                        object size = precompiled.Pop();

                        if (size.GetType() == typeof(int))
                        {
                            size = (int)size;
                        }

                        if (type == "Object" || type == "Array")
                        {
                            precompiled.Push(new object());
                        }

                        break;
                    }

                // Access to the writer if exist
                case ActionCode.GetVariable:
                    {
                        string variable = (string)precompiled.Pop();

                        // try to access to the object
                        try
                        {
                            writer = writers[variable];
                        }
                        catch
                        {
                            writer = writers["root"];
                        }
                        break;
                    }

                // Access to the writer if exist
                case ActionCode.GetMember:
                    {
                        object variable = precompiled.Pop();

                        if (variable.GetType() == typeof(int))
                        {
                            variable = variable.ToString();
                        }
                        else if (variable.GetType() == typeof(double))
                        {
                            variable = variable.ToString();
                        }

                        string parent = writers.FirstOrDefault(x => x.Value == writer).Key;
                        string writerName = parent + '.' + (string)variable;

                        // try to access to the object
                        try
                        {
                            writer = writers[writerName];
                        }
                        catch
                        {
                            writer = writers[parent];
                        }
                        break;
                    }

                // Not used, so skip this part
                case ActionCode.CallFunction:
                    {
                        precompiled.Pop();
                        precompiled.Pop();
                        break;
                    }
            }
        }

        // Used for single Array too
        private void WriteMultiDimensionnalArray(object[] value)
        {
            writer.WriteStartArray();

            foreach (object o in value)
            {
                if (o.GetType() == typeof(object[]))
                {
                    WriteMultiDimensionnalArray((object[])o);
                }
                else
                {
                    if (o.GetType() == typeof(JObject))
                    {
                        JObject v = (JObject)o;
                        writer.WriteToken(v.CreateReader());
                    }
                    else
                    {
                        writer.WriteValue(o);
                    }
                }
            }

            writer.WriteEndArray();
        }

        // Used for single Array too
        private JArray GetMultiDimensionnalArray(object[] value)
        {
            JArray array = new JArray();

            foreach (object o in value)
            {
                if (o != null)
                {
                    if (o.GetType() == typeof(object[]))
                    {
                        array.Add(GetMultiDimensionnalArray((object[])o));
                    }
                    else
                    {
                        array.Add(o);
                    }
                }
                else
                {
                    array.Add(JValue.CreateNull());
                }

            }
            return array;
        }

        private JObject InitObject(int size)
        {
            JObject obj = new JObject();

            object value;
            string name;

            for (int i = 0; i < size; i++)
            {
                value = precompiled.Pop();
                name = precompiled.Pop().ToString();

                if (value != null)
                {
                    if (value.GetType() == typeof(object[]))
                    {
                        value = GetMultiDimensionnalArray((object[])value);
                    }
                    // New Object, store to future use
                    else if (value.GetType() == typeof(object))
                    {
                        value = new JObject();
                    }
                }

                obj.Add(new JProperty(name, value));
            }

            return obj;
        }

        // Return primitive object of Push Items
        private object GetValuePushItem(ActionPushItem item)
        {
            switch (item.Type)
            {
                // Get the string constant a the index < 256
                case ActionPushItemType.Constant8:
                    {
                        return constants[item.Constant8];
                    }
                // Get the string constant a the index >= 256
                case ActionPushItemType.Constant16:
                    {
                        return constants[item.Constant16];
                    }
                case ActionPushItemType.Boolean:
                    {
                        return item.Boolean == 1;
                    }
                case ActionPushItemType.Double:
                    {
                        return item.Double;
                    }
                case ActionPushItemType.Float:
                    {
                        return item.Float;
                    }
                case ActionPushItemType.Integer:
                    {
                        return item.Integer;
                    }
                case ActionPushItemType.String:
                    {
                        return item.String;
                    }
                case ActionPushItemType.Register:
                    {
                        return null;
                    }
                case ActionPushItemType.Undefined:
                    {
                        return null;
                    }
                case ActionPushItemType.Null:
                    {
                        return null;
                    }
                default:
                    {
                        return null;
                    }
            }
        }
        #endregion
    }
}
