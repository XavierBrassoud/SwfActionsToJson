using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Newtonsoft.Json.Linq;
using FluentAssertions;

namespace XavLab.SwfUtils.Tests
{
    [TestClass]
    public class SwfActionsToJsonTests
    {
        [TestMethod]
        public void GetJson_WithValidSwfFile_ReturnJObject()
        {
            string path = @"Resources/sample.swf";
            Stream file = File.Open(path, FileMode.Open, FileAccess.Read);
            JObject expectedJson = new JObject
            {
                { "MyVar", "Hello World!" }
            };

            SwfActionsToJson swfActionsToJson = new SwfActionsToJson(file);
            JObject json = swfActionsToJson.GetJson();

            json.Should().Equal(expectedJson);
        }
    }
}
