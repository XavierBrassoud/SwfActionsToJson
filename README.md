# SwfActionsToJson .NET

[![Pipeline Status](https://gitlab.com/XavierBrassoud/SwfActionsToJson/badges/master/pipeline.svg)](https://gitlab.com/XavierBrassoud/SwfActionsToJson/commits/master)
[![Nuget Package](https://img.shields.io/nuget/v/SwfActionsToJson.svg?style=default)](https://www.nuget.org/packages/SwfActionsToJson/)
[![License: MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](./LICENSE)

<br/>

SwfActionsToJson is a simple library developed with the **.NET Standard 2.0** which serializing ActionScript code from a 
SWF file into a formatted JSON object.

<br/>

## Installation
Either checkout this GitLab repository or install `SwfActionsToJson` via *NuGet Package Manager*.
If you want to use *NuGet*, just search for "SwfActionsToJson" or run the following command in the *NuGet Package Manager Console*:

```
PM> Install-Package SwfActionsToJson
```

<br/>

## Demonstration

*Input* (from ActionScript of SWF file) :
``` actionscript
MyVar = "Hello World!";

MyObject = new Object();
MyObject.One = 1;
MyObject.Two = "Two";

MyArray = [ 1, 2, 3];
MyMultiDimensionnalArray = [[ 1, 2, 3], [ 4, 5, 6]];

// This line is skipped (unable to parse in JSON)
Foo.bar(42);

```

*Output* :
``` json
{
  "MyVar": "Hello World!",
  "MyObject": {
    "One": 1,
    "Two": "Two"
  },
  "MyArray": [
    1,
    2,
    3
  ],
  "MyMultiDimensionnalArray": [
    [
      1,
      2,
      3
    ],
    [
      4,
      5,
      6
    ]
  ]
}

```

<br/>

## Usage example

A simple C# console application that convert your SWF file in JSON file.

``` c#
using Newtonsoft.Json.Linq;
using System;
using System.IO;

using XavLab.SwfUtils;


static void Main(string[] args)
{
    // Read SWF file
    string path = "path\file.swf";
    Stream file = File.Open(path, FileMode.Open, FileAccess.Read);
    SwfActionsToJson converter = new SwfActionsToJson(file);

    // Store in JSON object
    JObject obj = converter.GetJson();

    // Write the result in JSON file
    File.WriteAllText("path\file.json", obj.ToString());
}

```

<br/>

## Meta

[@XavierBrassoud](https://gitlab.com/XavierBrassoud) - 2018

SwfLib : https://github.com/SavchukSergey/SwfLib <br/>
Json.NET : https://www.newtonsoft.com/json