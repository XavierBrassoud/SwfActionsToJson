# Release History

* 1.1.0
  * Fixing NullReferenceException when create JSON object with null value.
* 1.0.3
  * Using new version of [SwfLib](https://www.nuget.org/packages/SwfLib) (v1.0.2) instead of XavLab.SwfLib
* 1.0.2
  * Using [XavLab.SwfLib](https://gitlab.com/XavierBrassoud/XavLab.SwfLib) instead of [SwfLib](https://github.com/SavchukSergey/SwfLib).
* 1.0.1
  * Changing `SwfActionsToJson` namespace to a more conventional namespace `XavLab.SwfUtils`.
* 1.0.0
  * First release.